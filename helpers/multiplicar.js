// const fs = require("fs");
const colors = require('colors/safe');

const { writeFile, writeFileSync } = require("fs");




const crearTabla = async (base, limite, show) => {

    let consola = `=====================
    Tabla del: ${base}
${colors.red('=====================')}
`;

    let tabla = `=====================
    Tabla del: ${base}
=====================
`;

    for (let i = 1; i <= limite; i++) {
        tabla += `      ${base} x ${i} = ${i * base}
`;
        consola += `    ${base} ${colors.yellow('x')} ${i} = ${i * base}
`;
    }
    if (show) {

        console.log((consola));
    }
    colors.disable();

    writeFileSync(`tabla_del_${base}.txt`, tabla);
    return `tabla_del_${base}.txt`;

    // console.log(`El archivo tabla_del_${base}.txt ha sido salvado`);

    // fs.writeFile(`tabla_del_${base}.txt`, tabla, err => {
    //     if (err) throw err;
    //     console.log("El archivo ha sido salvado");
    // });


}


const crearTablaPro = async (base) => {


    return new Promise((resolve, reject) => {

        let tabla = `=====================
        Tabla del: ${base}
    =====================
    `;

        for (let i = 1; i <= 10; i++) {
            tabla += `      ${base} x ${i} = ${i * base}
    `;

        }
        try {
            writeFileSync(`tabla_del_${base}.txt`, tabla);
            resolve(`tabla_del_${base}.txt`)
        } catch (error) {
            reject(error)
        }

    })
}
module.exports = { crearTabla, crearTablaPro };