const argv = require('yargs')
    .options({
        'l': { alias: 'limite', demandOption: true, default: 10, describe: 'maxima multiplicación', type: 'number' },
        'b': { alias: 'base', demandOption: true, default: 3, describe: 'Base de la multiplicación', type: 'number' },
        's': {
            alias: 'show',
            type: 'boolean',
            default: false,
            describe: 'muestra la tabla en consola'
        }

    })
    .argv
    ;

module.exports = argv;